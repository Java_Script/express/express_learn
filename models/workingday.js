'use strict';
module.exports = (sequelize, DataTypes) => {
  const WorkingDay = sequelize.define('WorkingDay', {
    name: DataTypes.STRING,
    date: DataTypes.STRING
  }, {});
  WorkingDay.associate = function(models) {
    // associations can be defined here
  };
  return WorkingDay;
};