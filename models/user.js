'use strict'
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    fullName: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    roleId: DataTypes.INTEGER
  }, {})
  User.associate = (models) => {
    User.belongsTo(models.Role, {
      foreignKey: 'roleId'
    })
  }
  return User
}
