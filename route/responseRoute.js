'use strict'

const express = require('express')
const router = express.Router()
const responseController = require('../controllers/responseController')

router.get('/', responseController.getResponse)
router.get('/json', responseController.getJson)

module.exports = router
