'use strict'

const express = require('express')
const router = express.Router()
const homeController = require('../controllers/homeController')

router.get('/', homeController.home)
router.get('/register', homeController.register)
router.get('/login', homeController.login)
router.get('/logout', homeController.logout)

module.exports = router
