'use strict'

exports.home = (req, res) => {
  res.send('home')
}

exports.login = (req, res) => {
  res.send('login')
}

exports.logout = (req, res) => {
  res.send('logout')
}

exports.register = (req, res) => {
  res.send('register')
}
