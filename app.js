// 1. nmp init
// 2. nmp install --save-dav eslint
// 3. npx eslint --init
// 4. npm install --save express

'use strict'

// init
const express = require('express')
const app = express()
const path = require('path')

// view engine setup, optional for view or frontend
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// Handle request
app.use(express.json())

// Router init
const homeRouter = require('./route/homeRoute')
const responseRouter = require('./route/responseRoute')

// route
app.use('/home', homeRouter)
app.use('/response', responseRouter)

app.listen(3000)
